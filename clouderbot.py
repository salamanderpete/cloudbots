import requesocks as requests
import urllib
import re
import time
import sys
import botting

login_url = 'https://www.getclouder.com/login'

deploy_url = 'https://my.getclouder.com/order'
reg_url = "https://www.getclouder.com/signup"

result_url = 'https://my.getclouder.com/containers'
container_headers={ "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8", "Content-Type": "application/x-www-form-urlencoded", "Origin": "https://my.getclouder.com", "Referer": "", "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36" }
default_headers= {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'Accept-Encoding': 'gzip,deflate', 'Accept-Language': 'en-US,en;q=0.8', 'Cache-Control': 'max-age=0', 'Connection': 'keep-alive', 'Content-Type': 'application/x-www-form-urlencoded', 'Origin': 'https://www.getclouder.com', 'Referer': 'https://www.getclouder.com/signup', 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'} 
container_data = { 'app_domain': '', 'app_password': '', 'app_username': '', 'bw': '2', 'cpu': '2', 'custom_image_id': '', 'datacenter_id': '1', 'hdd': '20', 'hostname': 'happy_container_box', 'image_id': 'Ubuntu Trusty 14.04', 'image_type': '1', 'mem': '2', 'password': 'superContain123', 'password2': 'superContain123', 'password_type': 'own', 'predefined_plan': 'base', 'tos_agree': '1'}

session = requests.Session()
session.proxies = {'http': 'socks5://127.0.0.1:9050', 'https': 'socks5://127.0.0.1:9050'}

def get_token(url): 
   if 'order' in url:
      s = session.get(url).text
 #     generated_match = re.search(r'input\s.+generated_password.*\s*value="(.+)"', s)
#      generated_match = re.search(r'id=.*"generated_password"\s.*\s?value="(.+)"', s)
      print r'input[.\s]+"generated_password"[.\s]+value="(.+)"'
      generated_match = re.search(r'input[.\s]+"generated_password"[.\s]+value="(.+)"', s)
      token = re.search(r'hidden"\s(?:value="1")?name="([a-f0-9]+)', s).groups()[0]
      generated_password = generated_match.groups()[0]
      print generated_password
      return token, generated_password
   else: 
      return re.search(r'hidden"\s(?:value="1")?name="([a-f0-9]+)', session.get(url).text).groups()[0]

def get_clouder_post_data(d, url):
   d[get_token(url)] = 1
   return urllib.urlencode(d)



def clouder_form(email, password, url):
   params = {'email' : email, 'password' : password}
   data= get_clouder_post_data(params, url)
   print data
   r= session.post(url, data=data, headers=default_headers)
   success =  re.search(r'(welcome.*)', r.text) is not None
   return r

def register(email, password):
   return clouder_form(email, password, reg_url)

def login(email, password): return clouder_form(email, password, login_url)

def clouder_deploy(email, password):
   login(email, password)
   time.sleep(5)
#   container_data['host_name'] = email
#   container_data['password'] = container_data['password2'] = password
   token, gen_pw = get_token(deploy_url)
   container_data[token] = 1
   container_data['generated_password'] = gen_pw
   print container_data
   data = urllib.urlencode(container_data)
   r= session.post(deploy_url, data=data, headers=container_headers)
#   return r
   ip_re = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
   print email, '  ', password
   r=s.get(result_url)
   result_ip = re.search(ip_re, r.text).groups()[0]
   print 'resul_ip', result_ip, 'password', 'superConatin123'


def go_hard(email, password):
   botting.tor_switch()
   time.sleep(5)
   register(email, password)
   time.sleep(60)
   clouder_deploy(email, password)


if __name__ == '__main__': 
   print len(sys.argv)
   if len (sys.argv) > 2: 
      go_hard(sys.argv[1], sys.argv[2])
