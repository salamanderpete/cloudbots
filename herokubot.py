import time
from data.mymongo import MyMongo
import requesocks
from botting import *
import sys
import re
import heroku



m = MyMongo()

def getsession():
    session = requesocks.Session()
    session.proxies = {'http': 'socks5://127.0.0.1:9050',    'https': 'socks5://127.0.0.1:9050'}
    return session

def herokureg(email):
  session = getsession()
  formurl='https://id.heroku.com/signup'
  posturl='https://id.heroku.com/account'
  headers = {'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' , \
'Content-Type' : 'application/x-www-form-urlencoded' , 'Origin' : 'https://id.heroku.com' , \
'Referer' :  'https://id.heroku.com/signup', 'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.21 (KHTML, like Gecko) QupZilla/1.4.4 Safari/537.21' }
  regex = r'name="_csrf".+value="((?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?)\"' #matches base64
  r = session.get(formurl)
  match = re.search(regex, r.text)  #fetch the token we need for our post. It's a hidden field in their form.
  if match: csrf_token=match.groups()[0]
  else: return
  params = {'email' : email, '_csrf' : csrf_token }
  r= session.post(posturl, headers=headers, params=params)
  m.botdb.insert({'email' : email,  'activated' : False, 'targetDomain' : 'heroku.com'})
  

def make_accounts(num_accounts, sleep_interval):
    for i in range(num_accounts):
        herokureg(get_email()) 
        tor_switch()
        time.sleep(sleep_interval)


def create_apps(email, pw):
  cloud = heroku.from_pass(email, pw)
  for i in range(0, random.randint(1,2)):
    app = cloud.apps.add()
  

# This only works when ssh key has been added maybe? I dunno
def deploy_tars(email, pw):
  posturl = 'https://api.heroku.com/apps' # platform API
  key = heroku.get_key(email, pw)
  cloud = heroku.from_key(key)
  headers = { 'Accept': 'application/vnd.heroku+json; version=3' , 'Authorization' : key} # from heroku.get_key
  posturl = 'https://api.heroku.com/apps' # platform API
  tarurl='https://github.com/mikepanciera/blank/raw/master/wf.tar.gz'
  j = json.dumps({'source_blob' : { 'url' : tarurl, 'version' : 'v0'} })
  for app in cloud.apps:
    buildurl = '/'.join([posturl, str(app.name), 'builds'])
    requests.post(buildurl, data=j, headers=headers)  #deploy from Tar file using platform API





if __name__ == '__main__':
  print len(sys.argv)
  try:  num_accounts, sleep_interval = int(sys.argv[1]), int(sys.argv[2])
  except: 
    print 'expect: python herokubot.py <num_accounts> <sleep_interval>'
    sys.exit(0)

  make_accounts(num_accounts, sleep_interval)

