from stem import Signal
from stem.control import Controller


def tor_switch(): 
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate()
        controller.signal(Signal.NEWNYM)
