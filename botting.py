from data.mymongo import MyMongo
import random
import time
import string
from stem import Signal
from stem.control import Controller


subdomains = [d['name'] for d in MyMongo().database.subdomains.find()]


def tor_switch(): 
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate()
        controller.signal(Signal.NEWNYM)

def get_email(): 
#    x = random.randint(0, min(len(peoplenames ) -1 ,len( words ) - 1)) 
#    fn, ln = peoplenames[x - 1], peoplenames[x] 
    username = random.choice(usernames)
    if len(username) > 11: username = username[:12]
#    username  += str(random.randint(0, 9999)) 
#    pw = get_password #[:17] # max is 16 chars+ random.randint(0, 5)] 
    email  = '@'.join([username, getemaildomain()]) 
    return email



def get_password():
    length = 10
    alphabet = string.ascii_letters + string.digits + '!@#$%^&*()'
    return ''.join(random.choice(alphabet) for i in range(length))

def getemaildomain(): 
    return random.choice(subdomains)

f = open('data/words.txt', 'r')
lines = f.readlines()
random.seed(time.time())
chars = ['_', '-', '.']
words = []
for line in lines: 
    char = chars[random.randint(0,2)] 
    line = line.replace(' ', char) 
    line = line.replace('/', '') 
    line = line.replace('"', '')
    line = line.replace("'", '')
    line = line.strip()
# if len(line) > 7: l.append(line) 
    words.append(line)



f.close()
pf = open('data/usernames.txt', 'r')
usernames = []
for line in pf.readlines():
    usernames.append(line.strip())
    pf.close()


