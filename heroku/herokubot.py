import time
import requesocks
from sneaky import tor_switch
from botting import *

def herokureg(email):
  formurl='https://id.heroku.com/signup'
  posturl='https://id.heroku.com/account'
  headers = {'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' , \
'Content-Type' : 'application/x-www-form-urlencoded' , 'Origin' : 'https://id.heroku.com' , \
'Referer' :  'https://id.heroku.com/signup', 'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.21 (KHTML, like Gecko) QupZilla/1.4.4 Safari/537.21' }
  regex = r'name="_csrf".+value="((?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?)\"' #matches base64
  r = session.get(formurl)
  match = re.search(regex, r.text)  #fetch the token we need for our post. It's a hidden field in their form.
  if match: csrf_token=match.groups()[0]
  else: return
  params = {'email' : email, '_csrf' : csrf_token }
  return session.post(posturl, headers=headers, params=params)


def make_accounts(num_accounts, sleep_interval):
    session = requesocks.Session()
    session.proxies = {'http': 'socks5://127.0.0.1:9050',    'https': 'socks5://127.0.0.1:9050'}


    for i in range(num_accounts):
        herokureg(get_email())
        tor_switch()
        time.sleep(sleep_interval)
