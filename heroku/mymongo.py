import pymongo
import urlparse 
import os 


class MyMongo:
  def __init__(self):

    MONGO_URL = os.environ.get('MONGOHQ_URL')
    if MONGO_URL:
      self.connection =connection =  pymongo.Connection(MONGO_URL)
      self.database= database = connection[urlparse.urlparse(MONGO_URL).path[1:]]
      self.botdb = database.botdb
      self.providers = database.providers
      self.domains = database.domains
    else:
        print 'Define MONGO_URL in Environ'
        return

  def close():
    self.connection.close()
