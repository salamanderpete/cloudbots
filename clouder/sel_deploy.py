# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re


class SelDeploy(unittest.TestCase):
        def setUp(self):
            self.driver = webdriver.Firefox()
            self.driver.implicitly_wait(30)
            self.base_url = "https://www.getclouder.com"
            self.verificationErrors = []
            self.accept_next_alert = True
            self.ip_re=  re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
        def test_dotest(self):
           self.sel_deploy(peace@benefits.drive.stelofme.ru, peace@benefits.drive.stelofme.ru) #name and login can be the same
     
        def _do_deploys(self):
           accounts = self.get_clouder_accounts()
           for a in accounts:
              self.sel_deploy(a[email], a[password])
     
        def get_instance_name(self): return benefitsdrivestelo
     
        def get_clouder_accounts():
           return list(m.botdb.find({activated : True, targetDomain : getclouder.com}))
     
        def sel_deploy(self, email, password):
            instance_name = self.get_instance_name()
            driver = self.driver
            driver.get(self.base_url + "/login")
            driver.find_element_by_id("email").send_keys(email)
            driver.find_element_by_id("password").send_keys(password)
            driver.find_element_by_id("loginSubmit").click()
            time.sleep(2)
     
            driver.get(https://my.getclouder.com/order)
            time.sleep(2)
         #   driver.find_element_by_link_text("Launch Container").click()
            driver.find_element_by_id("hostname").clear()
            driver.find_element_by_id("hostname").send_keys(instance_name)
            # ERROR: Caught exception [Error: Dom locators are not implemented yet!]
            print done, next get
            print tried new get
            driver.find_element_by_name("password_type").click()
            driver.find_element_by_id("password").clear()
            driver.find_element_by_id("password").send_keys(password)
            driver.find_element_by_id("password2").clear()
            driver.find_element_by_id("password2").send_keys(password)
            driver.find_element_by_xpath("//li[@value=Ubuntu Trusty 14.04]").click()
            driver.find_element_by_name("tos_agree").click()
            driver.find_element_by_id("submitClouder").click()
            driver.find_element_by_xpath("(//a[contains(text(),Continue)])[2]").click()
            # fetch ip from source
            time.sleep(6)
            match = re.search(self.ip_re, driver.page_source)
            if match:
               instance_ip = match.groups()[0]
               print ip, instance_ip
     
            print email, password, instance_ip, instance_name
     
    #        self.add_to_mongo(instance_name, instance_ip, email, password)
     
          #  driver.find_element_by_link_text("API").click()
          #  driver.find_element_by_link_text("Generate new key").click()
            #fetch API key from source
       
        def add_to_mongo(name, ip, email, pw):
           m.db.nodes.insert({instanceName : name, ip : ip, provider : getclouder.com, email : email, password : password})
     
     
        def is_element_present(self, how, what):
            try: self.driver.find_element(by=how, value=what)
            except NoSuchElementException, e: return False
            return True
       
        def is_alert_present(self):
            try: self.driver.switch_to_alert()
            except NoAlertPresentException, e: return False
            return True
       
        def close_alert_and_get_its_text(self):
            try:
                alert = self.driver.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally: self.accept_next_alert = True
       
        def tearDown(self):
            self.driver.quit()
            self.assertEqual([], self.verificationErrors)
     
    if __name__ == "__main__":
        unittest.main()


